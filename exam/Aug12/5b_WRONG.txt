\[
\begin{array}{rrrrrrrrrrrrrr}
x_{5} &= &5 &-&1.7 x_{1} &-&2.5 x_{2} &-&1.3 x_{3} &-&2 x_{4} \\
\hline 
z     &= &  &&32   x_{1} &+&40 x_{2}  &+&25  x_{3} &+&31 x_{4}
\end{array}
\]
Da vi har smidt vores integralitets begr�nsning ud, kan vi have decimale
l�sninger.
Da problemet er s� simpelt som det er tilf�ldet, skal vi blot finde den variabel, 
der har det bedste forhold imellem optimeringsv�rdien og begr�nsingen; alts�;
\begin{align*}
d_{1} &= \frac{32}{1.7} &\simeq 18.82 \\
d_{2} &= \frac{40}{2.5} &\simeq 16    \\
d_{3} &= \frac{25}{1.3} &\simeq 19.23 \\
d_{4} &= \frac{31}{2}   &\simeq 15.5
\end{align*}
Alts� i vores tilf�lde;
\[
d_{3} = max\left(d_{i}\right) \; | \; i \in \{1,2,3,4\}
\]
Nu skal vi blot finde ud af hvor meget af $v_{3}$ vi kan f�, givet vores
begr�nsing; dette g�res ved at s�tte alle andre $v_{i} = 0$, og finde vores
maximum; I vores tilf�lde;
\[
\frac{5}{1.3} \simeq 3.85
\]
Vi f�r da vores maximum $z$ ved;
\[
z = 25 * 3.85 = 96.25
\]
For det generelle tilf�lde, skal vi dog k�re simplex algorithmen for at finde
dette.
